#!/usr/bin/env python
import argparse
import csv
import numpy as np
import os
from statsmodels.sandbox.stats.runs import mcnemar
import time
from random import shuffle

from sklearn import svm
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import load_iris
from sklearn.cross_validation import StratifiedShuffleSplit
from sklearn.grid_search import GridSearchCV

def load_file(path):
    output = []
    with open(path, 'r') as f:
        reader = csv.reader(f, delimiter='\t')
        for row in reader:
            output.append((row[0], row[1], row[2] == 'True'))
    return output

def load_vectors(vectors):
    output = {}
    with open(vectors, 'r') as f:
        for line in f:
            word, vector = line.split(' ', 1)
            vector = np.fromstring(vector.rstrip(), sep=' ')
            output[word] = vector
    return output

def get_targets(pairs):
    output = set()
    for a,b,t in pairs:
        output.add(a)
        output.add(b)
    return output

def compute_target_presence(vectors, data):
    targets = get_targets(data)
    seen = 0
    for target in targets:
        if target in vectors:
            seen += 1
    print "Target presence is %0.3f out of %d" % (float(seen) / len(targets), len(targets))
    t = 0.0
    for point in data:
        if point[2]:
            t += 1
    print "Percent TRUE labels is %0.3f" % (t / len(data))

# Remove data points that aren't in the vectors.
def prune_data(vectors, data):
    output = []
    for point in data:
        if point[0] in vectors and point[1] in vectors:
            output.append(point)
    return output

def count_classes(bless):
    output = {}
    for pair in bless:
        c = bless[pair][CLASS_IND]
        if c in output:
            output[c] += 1
        else:
            output[c] = 1
    return output

def train_svm(data, vectors, compose, kernel='linear'):
    features = []
    classes = []
    num_true = 0.0
    for a,b,t in data:
        features.append(compose((a,b),vectors))
        classes.append(1 if t else 0)
        if t:
            num_true += 1
    true_class_weight = len(data) / num_true - 1.0
    class_weight = {1: true_class_weight}
    if kernel == 'rbf':
        svc = svm.SVC(kernel=kernel,\
          #C=4.6415888336127775,gamma=.10000000000000001)
          C=21.544346900318821,gamma=1.0)
    elif kernel == 'linear':
        svc = svm.LinearSVC(class_weight=class_weight)
    elif kernel == 'sim':
        svc = svm.SVC(kernel=kernel_sim, class_weight=class_weight)
    svc.fit(features, classes)
    return svc

def test_svm(data, vectors, svc, compose):
    output = svc.predict(\
      [compose(pair,vectors) for pair in data])
    ground_truth = [point[2] for point in data]
    true_pos =\
      sum([output[i] + ground_truth[i] == 2 for i in range(len(output))])
    precision = true_pos / float(sum(output))
    recall = true_pos / float(sum(ground_truth))
    f1 = 2 * precision * recall / (precision + recall)
    return f1

def compare_svm(data, vectors, svc, alt_vectors, alt_svc, compose):
    output = svc.predict(\
      [compose(pair,vectors) for pair in data])
    alt_output = alt_svc.predict(\
      [compose(pair,alt_vectors) for pair in data])
    ground_truth = [point[2] for point in data]
    # Compute F scores.
    agreements = [output[i] + ground_truth[i] == 2 for i in range(len(output))]
    true_pos = sum(agreements)
    precision = true_pos / float(sum(output))
    recall = true_pos / float(sum(ground_truth))
    f1 = 2 * precision * recall / (precision + recall)
    alt_agreements = [alt_output[i] + ground_truth[i] == 2 for i in range(len(alt_output))]
    alt_true_pos = sum(alt_agreements)
    precision = alt_true_pos / float(sum(alt_output))
    recall = alt_true_pos / float(sum(ground_truth))
    alt_f1 = 2 * precision * recall / (precision + recall)
    # Check significance.
    """
    success = 0
    fail = 0
    for i in range(len(agreements)):
        if agreements[i] == 0 and alt_agreements[i] == 1:
            success += 1
        if agreements[i] == 1 and alt_agreements[i] == 0:
            fail += 1
        else:
            success += 0.5
            fail += 0.5
    success = int(np.ceil(success))
    fail = int(np.ceil(fail))
    return f1, alt_f1, scipy.stats.binom_test(success, n=fail+success)
    """
    _, p = mcnemar(agreements, alt_agreements, exact=True)
    return f1, alt_f1, p

def vector_diff(pair, vectors):
    return vectors[pair[1]] - vectors[pair[0]]

def vector_y(pair, vectors):
    return vectors[pair[1]]

def vector_concat(pair, vectors):
    return np.concatenate((vectors[pair[0]], vectors[pair[1]]))

def kernel_sim(A,B):
    x1, y1 = np.split(A, 2, axis=1)
    x2, y2 = np.split(B, 2, axis=1)
    alpha = 0.7
    zero_thresh = 1e-12
    inter_y = np.add.outer(np.einsum('ij,ij->i',y1,y1), np.einsum('ij,ij->i',y2,y2))\
        - 2 * np.dot(y1, y2.T)
    inter_x = np.add.outer(np.einsum('ij,ij->i',x1,x1), np.einsum('ij,ij->i',x2,x2))\
        - 2 * np.dot(x1, x2.T)
    inter = np.absolute(np.multiply(inter_x, inter_y))
    diff1 = x1 - y1
    intra1 = np.einsum('ij,ij->i',diff1,diff1)
    diff2 = x2 - y2
    intra2 = np.einsum('ij,ij->i',diff2,diff2)
    intra = np.outer(intra1,intra2)
    return np.multiply(np.power(intra, alpha / 2), np.power(inter, (1 - alpha)/ 2))

def run_gridsearch(data, vectors, compose):
    C_range = np.logspace(-2, 6, 13)
    gamma_range = np.logspace(-9, 3, 13)
    data_vectors = [compose(point,vectors) for point in data]
    labels = [(1 if point[2] else 0) for point in data]
    param_grid = dict(gamma=gamma_range, C=C_range)
    cv = StratifiedShuffleSplit(labels, n_iter=5, test_size=0.2, random_state=42)
    grid = GridSearchCV(svm.SVC(), param_grid=param_grid, cv=cv, verbose=2)
    grid.fit(data_vectors, labels)
    print("The best parameters are %s with a score of %0.2f"
          % (grid.best_params_, grid.best_score_))

def run_svm(data_path, vectors_path, kernel, gridsearch, alt_vectors_path, split):
    vectors = load_vectors(vectors_path)
    if alt_vectors_path:
        alt_vectors = load_vectors(alt_vectors_path)
    compositions = [vector_diff, vector_concat]
    for root, dirnames, filenames in os.walk(data_path):
        #if not ('baroni' in root or 'bless' in root):
        #    continue
        if len(dirnames) > 0:
            continue
        for filename in filenames:
            if split in filename:
                data = load_file(os.path.join(root, filename))
                if 'train' in filename:
                    train_data = prune_data(vectors, data)
                    if gridsearch:
                        run_gridsearch(train_data, vectors, vector_diff)
                elif 'test' in filename:
                    test_data = prune_data(vectors, data)
                    #compute_target_presence(vectors, test_data)
        if kernel == 'sim':
            comp = vector_concat
        else:
            comp = vector_diff
        svc = train_svm(train_data, vectors, comp, kernel=kernel)
        if alt_vectors_path:
            alt_svc = train_svm(train_data, alt_vectors, comp, kernel=kernel)
            f1, alt_f1, p = compare_svm(test_data, vectors, svc, alt_vectors, alt_svc, comp)
            print "%s & %0.3f & %0.3f & %0.3f & %0.3f" % (root, f1, alt_f1, alt_f1 - f1, p)
        else:
            f1 = test_svm(test_data, vectors, svc, comp)
            print "F1 Score: %0.3f" % f1

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run SVM classifier of BLESS relations.")
    parser.add_argument('data', type=str, help='path to datasets')
    parser.add_argument('vectors', type=str, help='path to vectors')
    parser.add_argument('--compare', type=str, help='path to vectors to compare to')
    parser.add_argument('--kernel', type=str, help='type of kernel', default='linear')
    parser.add_argument('--split', type=str, help='type of split, rnd or lex', default='rnd')
    parser.add_argument('--gridsearch', action='store_true')
    args = parser.parse_args()
    run_svm(args.data, args.vectors, args.kernel, args.gridsearch, args.compare, args.split)

